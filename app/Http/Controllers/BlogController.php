<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;
use DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //función para insertar registro en base de datos
    public function addBlog(Request $request)
    {
        try {
            //conversión de los parametros enviados
            $data = json_decode(json_encode(json_decode($request->getContent(), true)));

            //inserción de registro
            $blog = Blog::create([
                'titulo' => $data->titulo,
                'autor' => $data->autor,
                'fecha_publicacion' => $data->fecha,
                'contenido' => $data->contenido,  
            ]);

            //retorno de registro 
            return response()->json([
                "response" => 200,
                "message" => "Registro agregado",
                "blog" => $blog
            ]);
        } catch (\Exception $th) {
            //retorno error al insertar
            return response()->json([
                "response" => 500,
                "message" => "Ocurrio un error al agregar el registro",
                "blog" => null,
            ]);
        }
    }

    //función para obtener registros
    public function getBlogs()
    {
        try {
            //consulta de todos los registros
            $blogs = Blog::all();

            //retorno de registros
            return response()->json([
                "response" => 200,
                "message" => "Listado de registros",
                "blogs" => $blogs,
            ])->header("Access-Control-Allow-Origin",  "*");
        } catch (\Throwable $th) {
            //retorno de error al obtener registros
            return response()->json([
                "response" => 500,
                "message" => "Ocurrio un error al obtener los registros",
                "blogs" => null,
            ]);
        }
    }

    //función para buscar blogs por medio de filtrado
    public function searchBlogs(Request $request)
    {
        try {
            //conversión de los parametros enviados
            $data = json_decode(json_encode(json_decode($request->getContent(), true)));
            //parámetro de búsqueda
            $search = $data->search;
            //query para filtrar por titulo, autor y contenido
            $query = "SELECT * FROM blog WHERE titulo LIKE '%".$search."%' OR autor LIKE '%".$search."%' OR contenido LIKE '%".$search."%' GROUP BY id";
            //respuesta de consulta
            $blogs = DB::select($query);
            //retorno de los registros
            return response()->json([
                "response" => 200,
                "message" => "Registro encontrados",
                "blogs" => $blogs
            ]);
        } catch (\Exception $th) {
            //retorno de error al filtrar
            return response()->json([
                "response" => 500,
                "message" => "Ocurrio un error al obtener registros",
                "blogs" => null,
            ]);
        }
    }

    public function getBlog($idBlog=null)
    {
        //control de excepción
        try {
            //búsqueda de blog por id
            $blog = Blog::find($idBlog);
            //retorno del registro del blog
            return response()->json([
                "response" => 200,
                "message" => "Buscar blog",
                "blog" => $blog,
            ])->header("Access-Control-Allow-Origin",  "*");
        } catch (\Throwable $th) {
            //retorno de error al buscar registro
            return response()->json([
                "response" => 500,
                "message" => "Ocurrio un error al obtener el registro",
                "blogs" => null,
            ]);
        }
    }
}
