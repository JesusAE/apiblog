<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    //nombre de la tabla
    protected $table = "blog";

    //llave primaria
    protected $primaryKey = 'id';

    //atributos de la tabla
    protected $fillable = [
        'titulo', 'autor', 'fecha_publicacion', 'contenido'
    ];
}
