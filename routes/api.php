<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

#inicio rutas de consumo de API Blog
Route::post('/addBlog', 'App\Http\Controllers\BlogController@addBlog');
Route::get('/getBlogs', 'App\Http\Controllers\BlogController@getBlogs');
Route::get('/getBlog/{idBlog}', 'App\Http\Controllers\BlogController@getBlog');
Route::post('/searchBlogs', 'App\Http\Controllers\BlogController@searchBlogs');
#fin rutas API Blog

